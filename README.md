# YOLO-19: A YOLO-based Face Mask Detection Experiment

This repository contains the Colaboratory notebook as well as the article for the Deep Learning course project.

## Usage

Simply open ```yolo19.ipynb``` using Colaboratory to inspect source code and original notebook. Please note the folders structure at the beginning of this file. Data files are available at: https://drive.google.com/drive/folders1UdeaQ91jplmbaaI_6PgCyA-z9KWZfZBX?usp=sharing

## Report

For deeper analysis, please see external W&B reports.

https://wandb.ai/dylanakh/YOLOv5/reports/Report-Exp-1-comparison---Vmlldzo0OTA5MDI
https://wandb.ai/dylanakh/YOLOv5/reports/Report-Exp-2-augmentation---Vmlldzo0OTA4OTQ

## License
Public License domain. I do not own any images in the provided dataset.}